// call the library : https://bitbucket.org/nx/std-builds-library

@Library('std-builds-library') _

pipeline {
    agent {
        kubernetes {
            inheritFrom 'maven-builder-jdk8'
            defaultContainer 'maven'
        }
    }

    environment {
            JAVA_TOOL_OPTIONS = "-Duser.timezone=Europe/Paris -Duser.language=FR -Duser.country=FR -Duser.variant=FR"
            HOME = "${WORKSPACE}"
            BINARIES_HOME = "/home/jenkins/agent/ta.embedded.3rdparty"            
    }
    parameters{
        choice(
            name : 'build_type',
            description : 'Choose between regular or release build',
            choices: ["regular", "release"]
        )

        string(
            name: "release_version",
            description: "the release version (if release)",
            defaultValue : ""
        )

        string(
            name: "next_version",
            description: "the next development version (if release)",
            defaultValue : ""
        )
    }    

    stages {
        stage('install dependencies') {
            steps {
                
               dir('/home/jenkins/agent/ta.embedded.3rdparty'){
                    withCredentials([usernamePassword(
                        credentialsId: 'jenkins-nexus-credentials',
                        passwordVariable: 'PASSWORD',
                        usernameVariable: 'USER')]
                        ) {
                            echo 'installation des packets'
                            sh 'apt-get install wget iputils-ping unzip -y'
                            echo 'installation des binaires tiers'
                            sh 'wget -q --user=${USER} --password=${PASSWORD} "https://nexus.squashtest.org/nexus/repository/third-party-binaries/ta.embedded/apache-maven-3.5.0.tar.gz"'  
                            sh 'wget -q --user=${USER} --password=${PASSWORD} "https://nexus.squashtest.org/nexus/repository/third-party-binaries/ta.embedded/grammar-kit-2017.1.7.jar"'  
                            sh 'tar -xvf apache-maven-3.5.0.tar.gz'
                            sh 'rm -Rf apache-maven-3.5.0.tar.gz' 
                        }
               
               }
                
            }
        }        
        stage('Build') {
            when  {
                expression { params.build_type == 'regular' }
                  }         
            
            steps {           
                    
                sh '''
                    mvn -B -ntp org.jacoco:jacoco-maven-plugin:prepare-agent deploy -Pci,public \
                        -Dtools.sourceDirectory=${BINARIES_HOME} 
                        
                    '''

            }
            post {
                success {
                    script {
                            echo 'publishing installers'
                            withCredentials([usernamePassword(
                                        credentialsId: 'jenkins-nexus-credentials',
                                        passwordVariable: 'PASSWORD',
                                        usernameVariable: 'USER')]
                                        ) {
                                            //uploading installers
                                            sh '''
                                            cd squash-ta-tools-bundle/target/
                                            for installer in $(find . -name "squash-ta-tools-bundle-*-installer.exe.jar"); \
                                            do curl -i --fail --user "$USER:$PASSWORD" --upload-file "$installer" "https://nexus.squashtest.org/nexus/repository/binaries-acceptance/squash-tf/squash-tf-1/$installer"; done
                                            '''                                                      
                                        } 
                    }
                }
            }             
        }
        stage('QA'){
            when  {
                expression { params.build_type == 'regular' }
                  }  
            steps {
                withSonarQubeEnv('SonarQube'){
                    sh 'mvn -Dsonar.scm.disabled=true sonar:sonar'
                }
                timeout(time: 10, unit: 'MINUTES'){
                    waitForQualityGate abortPipeline: false
                }
            }
            post {
                always{
                    script {
                        notifications.pushToGChat(['gchat_credentials' : 'tf-legacy-build-status'])
                    }
                }
            } 
        }
        // release : when the build is a release (serious game) build
        stage('release'){
            when{
                expression { params.build_type == 'release' }
            }
            steps {
                container('maven') {
                    script {

                        def valid = release.isConfValid()
                        if (!valid) {
                            error('Cannot release : either the release version or next dev version is wrong')
                        }

                        echo "releasing : "
                        
                        sh """
                            mvn -B -ntp release:prepare -Dpush=false release:perform \
                            -DuseReleaseProfile=false \
                            -Dmaven.source.skip \
                            -Dtools.sourceDirectory=${BINARIES_HOME} \
                            -Darguments=\" -Dtools.sourceDirectory=${BINARIES_HOME}\" \
                            -Pci,public"""                   
                        echo "release complete !"

                    }
                }

            }
            post{
                always{
                    script {
                        notifications.pushToGChat(['gchat_credentials' : 'tf-legacy-build-status'])
                    }
                }
                success {
                    script {
                                             
                        echo 'publishing installers'
                        withCredentials([usernamePassword(
                                    credentialsId: 'jenkins-nexus-credentials',
                                    passwordVariable: 'PASSWORD',
                                    usernameVariable: 'USER')]
                                    ) {
                                        script{
                                            echo 'choosing repository to upload installers'
                                            def repository_name = ''
                                            
                                            if(params.RELEASE_VERSION.contains('RELEASE')) {repository_name='public-releases'}
                                            //for the cases RC or others 
                                            else {repository_name='public-acceptance'}                                      
                                            
                                                                        
                                            //uploading installer
                                            sh """
                                            cd squash-ta-tools-bundle/target/
                                            for installer in $(find . -name "squash-ta-tools-bundle-*-installer.exe.jar"); \
                                            do curl -i --fail --user "\$USER:\$PASSWORD" --upload-file "\$installer" "https://nexus.squashtest.org/nexus/repository/${repository_name}/squash-tf/squash-tf-1/\$installer"; done
                                            """ 
                                                        
                                        }
                                    }                
                        release.addReleaseInfo(this)
                        release.push()
                    }
                }
            }
        }        
    }
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.installer.escaper;

import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author qtran
 */
public class EscaperTest {
        
    @Test
    public void backslashBeforeNonUnicodeAreDoubled(){
        String input = "\\u123";
        String expectedResult="\\\\u123";
        String actualResult = Escaper.doubleBackslashes(input);       
        
        Assert.assertEquals("Backslash before non-unicode are doubled.", expectedResult, actualResult);
    }
    
    @Test
    public void backslashBeforeUnicodeAreNotDoubled(){
        String input = "\\u1234";
        String expectedResult="\\u1234";
        String actualResult = Escaper.doubleBackslashes(input);
        Assert.assertEquals("Backslash before unicode are NOT doubled.", expectedResult, actualResult);
    }
    
    @Test
    public void modifiedEscapeUnicodeTest(){
        String input = "Les caractères spéciaux sont bien remplacés par les Unicode.";
        String expectedResult = "Les caract\\u00e8res sp\\u00e9ciaux sont bien remplac\\u00e9s par les Unicode.";
        String actualResult = Escaper.escapeUnicode(input);
        Assert.assertEquals("Special characters are NOT replaced by corresponding Unicode char!", expectedResult, actualResult);
    }
    
    @Test
    public void nonModifiedEscapeUnicodeTest(){
        String input = "Nothing to be changed here, baby.";
        String expectedResult = "Nothing to be changed here, baby.";
        String actualResult = Escaper.escapeUnicode(input);
        Assert.assertEquals("Normal characters are REPLACED by NON-corresponding Unicode char!", expectedResult, actualResult);
    }
}

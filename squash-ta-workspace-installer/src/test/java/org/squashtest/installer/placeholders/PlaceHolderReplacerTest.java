/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.installer.placeholders;

import java.io.File;
import java.io.IOException;
import org.junit.Before;
import org.junit.Test;
import org.squashtest.ta.api.test.toolkit.ResourceExtractorTestBase;

/**
 *
 * @author qtran
 */
public class PlaceHolderReplacerTest extends ResourceExtractorTestBase {

    private PlaceHolderReplacer testee;
    File src;
    File expectedResult;
    String stringSample;

    @Before
    public void setUp() throws IOException {
        src = createFile("source-file.txt");
        File rawExpectedResult=createFile("target-file.txt");
        expectedResult = toPlatformEndOfLine(rawExpectedResult);
        testee = new PlaceHolderReplacer("taToolboxPath:8080", "workspacePath:8080", "jdkPath:8080",
                "eclipseSubPath:8080", "mavenSubPath:8080");
    }

    @Test
    public void noProblemIfInputFileVariablesProperlyReplaced() throws IOException {

        testee.processFile(src.getAbsolutePath());
        checkActualContentAgainstExpected(src, expectedResult);
    }
}

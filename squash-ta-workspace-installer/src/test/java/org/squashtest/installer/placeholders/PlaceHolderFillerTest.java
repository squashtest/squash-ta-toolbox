/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.installer.placeholders;

import java.io.File;
import java.io.IOException;
import org.junit.Test;
import org.squashtest.ta.api.test.toolkit.ResourceExtractorTestBase;

/**
 *
 * @author qtran
 */
public class PlaceHolderFillerTest extends ResourceExtractorTestBase {

    private PlaceHolderFiller testee = new PlaceHolderFiller();

    @Test
    public void noProblemIfOutputFileIsReplacedByInputFileContentWithUpdates() throws IOException {
        //import the resources fonr File
        File src = createFile("source-file-2.txt");
        File dest = createNtrackFile();
        
        File expected = createFile("target-file-2.txt");

        //initiate the Input/OutputFile
        testee.setInputFile(src);
        testee.setOutputFile(dest);

        //processing file update
        testee.testCJUprocess();

        //test if the new target file has the same content with the source one
        checkActualContentAgainstExpected(dest, expected);
    }
}

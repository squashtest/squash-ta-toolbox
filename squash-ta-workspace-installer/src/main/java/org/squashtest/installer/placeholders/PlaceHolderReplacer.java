/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.installer.placeholders;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.installer.tools.ConvertPathTool;


public class PlaceHolderReplacer {

	private Map<String, String> replacementMap = new HashMap<String, String>();

	protected static final Logger LOGGER = LoggerFactory.getLogger(PlaceHolderReplacer.class);

	private File inputFile;
	private File outputFile;


	public PlaceHolderReplacer(String toolboxPath, String workspacePath, String jdkPath, String eclispeSubfolder,
			String mavenSubfolder) {

		// build paths
		String WORKSPACE_PATH_ESC = ConvertPathTool.convertPathToPathEsc(workspacePath);
		String JDK_PATH_ESC = ConvertPathTool.convertPathToPathEsc(jdkPath);
		String JDK_PATH_JAVA = ConvertPathTool.convertPathToJavaPath(jdkPath);
		String ECLIPSE_PATH = toolboxPath + "\\" + eclispeSubfolder;
		String ECLIPSE_DBSEPARATOR_PATH = ConvertPathTool.convertPathToDoubleBackslashesPath(ECLIPSE_PATH);
		String MAVEN_PATH_ESC = ConvertPathTool.convertPathToPathEsc(toolboxPath + "\\" + mavenSubfolder);

		replacementMap.clear();
		replacementMap.put("${WORKSPACE_PATH}", workspacePath);
		replacementMap.put("${WORKSPACE_PATH_ESC}", WORKSPACE_PATH_ESC);
		replacementMap.put("${MAVEN_FOLDER_NAME}", mavenSubfolder);
		replacementMap.put("${MAVEN_PATH_ESC}", MAVEN_PATH_ESC);
		replacementMap.put("${JDK_PATH_JAVA}", JDK_PATH_JAVA);
		replacementMap.put("${JDK_PATH_ESC}", JDK_PATH_ESC);
		replacementMap.put("${JDK_PATH}", jdkPath);
		replacementMap.put("${ECLIPSE_DBSEPARATOR_PATH}", ECLIPSE_DBSEPARATOR_PATH);
		replacementMap.put("${ECLIPSE_PATH}", ECLIPSE_PATH);

		LOGGER.info(" Replacement Map setting => key - value: ");
		for (String key : replacementMap.keySet()) {
			LOGGER.info("\t " + key + " - " + replacementMap.get(key));
		}
		LOGGER.info(" End Replacement Map setting ");		
	}

	public void processFile(String file) throws IOException  {
		//crate inputFile and outputFile
		inputFile = new File(file);
                outputFile = new File (file+".tmp");
                
                //action
		replaceInFile();
		//post handling
                
                boolean isDeleteOk = inputFile.delete();
		boolean isRenameOk = outputFile.renameTo(new File(file));
                
                if (!isDeleteOk || !isRenameOk) {
                    throw new IOException("Failed to replace original file with PlaceHolder by processed file.");
                }
	}

	private void replaceInFile()  {
		
		PrintStream outStream = null;
		try {
			outStream = new PrintStream(outputFile);
		} catch (FileNotFoundException e1) {
			LOGGER.error("PlaceHolderReplacer::replaceInFile Error in create new PrintStream for output file",e1);
		}
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(inputFile));
		} catch (FileNotFoundException e1) {
			LOGGER.error("PlaceHolderReplacer::replaceInFile Error in create BufferREader for input file",e1);
		}
		
		try{			
			String buffer;
			
			while((buffer=reader.readLine())!=null){
				String replaced = replace(buffer);
				outStream.println(replaced); 
			}
			outStream.close();
			reader.close();
			
		} catch (IOException e) {
			LOGGER.error("PlaceHolderReplacer::replaceInFile in read/write line",e); 
		}finally{
			outStream.close();
			try {
				reader.close();
			} catch (IOException e) {				
				LOGGER.error("Failed to close reader",e);
			}			
		}
	}
	
	private String replace(String original){
		String result = original;
		for (String key : replacementMap.keySet() ){
			if (result.contains(key)){
				result = result.replace(key, replacementMap.get(key));
				LOGGER.debug("replace " + key + "with " +replacementMap.get(key) );
			}
		}
		return result;
	}
}

/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.installer.placeholders;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;

/**
 * that class is plain stupid : it takes an input file, replace strings with others then write out the result.
 * There is currently only one supported place holder named USER_WORKSPACE_ESC, replaced by remplacement1OK
 *  
 * Note that both files must exist.
 * 
 * @author bsiri
 * 
 * TODO : refactor it to make it more flexible and less dependant of the purpose of the app. It may induce slight
 * modifications of the file processed if the names of the placeholders change.
 *
 */
public class PlaceHolderFiller {

	File inputFile;
	File outputFile;

	private Map<String, String> replacementMap = new HashMap<String, String>();
	
	public void setInputFile(File inputFile) {
		this.inputFile = inputFile;
	}
	
	public void setOutputFile(File outputFile) {
		this.outputFile = outputFile;
	}

        public Map<String, String> getReplacementMap(){
		return replacementMap;
	}
	
	public void testCJUprocess() throws IOException{
		//init de la map (chaine de remplacement)
		initMapReplacement();
		
		//GO
		replaceAndWrite();
	}
	
	private String replace(String original){
		String result = original;
		for (String key : replacementMap.keySet() ){
			if (result.contains(key)){
				result = result.replace(key, replacementMap.get(key));
			}
		}
		return result;
	}

	private void initMapReplacement() {
		replacementMap.clear();
		replacementMap.put("${USER_WORKSPACE_ESC}","remplacement1OK" );
        }
	
	private void replaceAndWrite() throws IOException {
	
		PrintStream outStream = new PrintStream(outputFile);
		BufferedReader reader = new BufferedReader(new FileReader(inputFile));
		
		try{			
			String buffer;
			
			while((buffer=reader.readLine())!=null){
				String replaced = replace(buffer);
				outStream.println(replaced);
			}
			outStream.close();
			reader.close();
			
		}finally{
			outStream.close();
			reader.close();			
		}
	}

}

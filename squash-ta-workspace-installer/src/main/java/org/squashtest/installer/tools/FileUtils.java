/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.installer.tools;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * that class does exist for the sole purpose of eliminating the dependencies to org.apache.commons.FileUtils
 * 
 * @author bsiri
 *
 */
public class FileUtils {
//        Utility classes, which are collections of static members, are not meant to be instantiated, so should not have public constructors.
//        Java adds an implicit public constructor to every class which does not define at least one explicitly.
//        Hence, we define a non-public constructor. 
        private FileUtils() {
            throw new IllegalStateException("Utility class");
        }
        
	public static void copyFile(File src, File dest) throws IOException{

		BufferedInputStream inStream = new BufferedInputStream(new FileInputStream(src));
		BufferedOutputStream outStream = new BufferedOutputStream(new FileOutputStream(dest));
		try{
			byte[] buffer = new byte[2048];			
			int count;
			
			while((count=inStream.read(buffer, 0, 2048))!=-1){
				outStream.write(buffer, 0, count);
			}
		}finally{
			inStream.close();
			outStream.close();
		}
		
	}
}

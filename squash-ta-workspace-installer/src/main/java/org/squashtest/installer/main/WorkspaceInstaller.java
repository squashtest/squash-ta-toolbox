/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2011 - 2019 Henix
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses />.
 */
package org.squashtest.installer.main;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.squashtest.installer.placeholders.PlaceHolderReplacer;
import org.squashtest.installer.tools.FileLocatorUtils;

/**
 * <p>
 * Will prepare a TA-oriented Eclipse workspace with preconfigured user
 * preferences
 * </p>
 * <p>
 * arguments :
 * </p>
 * <ul>
 * <li>-ta-directory : the home directory of Squash TA.</li>
 * <li>-dest : the place where the workspace will be installed.</li>
 * </ul>
 * 
 * <p>
 * The process is the following :
 * </p>
 * 
 * <ol>
 * <li>Replace placeholders in files</li>
 * <li>rename workspace folder directorries</li>
 * </ol>
 *  
 * <p>
 * Note that it uses an important classpath resource : the workspace_template.
 * It's an empty eclipse workspace. Due to build constraints, the .metadata
 * folder had to be rename 'metadata' (the dot disappeared). So as part of the
 * installation process the class have to rename it. Make sure that both
 * statements are true.
 * </p>
 * 
 * 
 * @author bsiri
 *
 */

public class WorkspaceInstaller {

	static Logger LOGGER;

	static {
		String path = System.getProperty("user.dir");
		System.setProperty("logFilename", path + File.separator + "SquashTA_Toolbox_Setup.log");
		LOGGER = LoggerFactory.getLogger(WorkspaceInstaller.class);
	}

	String taToolboxPath; 
	String workspacePath; 
	String jdkPath; 
	String eclipseSubPath; 
	String mavenSubPath;
	
	Boolean successInstall = true;

	List<String> fileListWithPlaceHolder = new ArrayList<String>();

	PlaceHolderReplacer replacer;
	FileLocatorUtils locatorUtils;
	PlaceHolderReplacer filler;
/**
 * 
 * @param args: paths of softwares installed
 * @throws IOException
 * @throws URISyntaxException
 */
	public WorkspaceInstaller(String... args) throws IOException, URISyntaxException {
		
		try {
			//parse commande line to get paths
			taToolboxPath = getOpt("-ta-directory", args);
			workspacePath = getOpt("-workspace-directory", args);
			jdkPath = getOpt("-jdkPath", args);
			eclipseSubPath = getOpt("-eclipse-subfolder", args);
			mavenSubPath = getOpt("-maven-subfolder", args);
		    
			// Build Map for placeHolder replacement
			replacer = new PlaceHolderReplacer(taToolboxPath, workspacePath, jdkPath,
					eclipseSubPath, mavenSubPath);

			// fill list of files to process
			initFileListWithPlaceHolder();
			LOGGER.info("Placeholder processing ..."); 
			for (String filePath :fileListWithPlaceHolder ) {
				LOGGER.info("  "+filePath + "  in process");
				replacer.processFile(filePath);
			}
			LOGGER.info("End Placeholder processing"); 
			
			Boolean ctrl = false;
			LOGGER.info("Workspace folders setting"); 
                        
                        final File wsFile = new File(workspacePath);
			
                        File work = new File(wsFile,"metadata");
			
                        ctrl = work.renameTo(new File(wsFile,".metadata"));
			
                        if (!ctrl) 
			{
				successInstall = false;
				LOGGER.error("Unable to rename 'metadata' folder, perhaps '.metadata' folder already exist"); 
			}
			File remote = new File(wsFile,"RemoteSystemsTempFiles/project.cfgxml");
			ctrl = remote.renameTo(new File(wsFile,"RemoteSystemsTempFiles/.project"));
			if (!ctrl) 
			{
				successInstall = false;
				LOGGER.warn("Unable to rename 'remoterystemstempfiles' folder, perhaps 'RemoteSystemsTempFiles' folder already exist"); 
			}
			LOGGER.info("End Workspace folders setting");  
			if (successInstall)
			{
				LOGGER.info(" ! Worskspace sucessively installed ! ");
				LOGGER.info(" Worskspacer installer closing. ");
			}
			else
			{
				LOGGER.error(" !ERROR:  Worskspace IS NOT installed ! ");
			}
			
		} catch (Exception e) {
			LOGGER.error(" ERROR: " ,e );
		}

	}
	
	private void initFileListWithPlaceHolder() {
		fileListWithPlaceHolder.clear();
		LOGGER.info("Setting Files list to process");
		// .metadata folfder for Eclipse - workspace
		File root = new File(workspacePath);
		fileListWithPlaceHolder.add(new File(root,"metadata/.plugins/org.eclipse.core.runtime/.settings/org.eclipse.m2e.core.prefs").getPath());
		//launches
		fileListWithPlaceHolder.add(new File(root,"metadata/.plugins/org.eclipse.debug.core/.launches/Run all tests.launch").getPath());
		fileListWithPlaceHolder.add(new File(root,"metadata/.plugins/org.eclipse.debug.core/.launches/Run selected test(s).launch").getPath());
		fileListWithPlaceHolder.add(new File(root,"metadata/.plugins/org.eclipse.debug.core/.launches/Run test list.launch").getPath());
		
		//launches for fwk before 1.9.0 (whithout log)
		fileListWithPlaceHolder.add(new File(root,"metadata/.plugins/org.eclipse.debug.core/.launches/(Before TA fwk 1.9.0) Run all tests.launch").getPath());
		fileListWithPlaceHolder.add(new File(root,"metadata/.plugins/org.eclipse.debug.core/.launches/(Before TA fwk 1.9.0) Run selected test(s).launch").getPath());
		fileListWithPlaceHolder.add(new File(root,"metadata/.plugins/org.eclipse.debug.core/.launches/(Before TA fwk 1.9.0) Run test list.launch").getPath());
		
		//launches for fwk before 1.7.1 (different goal)
		fileListWithPlaceHolder.add(new File(root,"metadata/.plugins/org.eclipse.debug.core/.launches/(Before TA fwk 1.7.0) Run all tests.launch").getPath());
		fileListWithPlaceHolder.add(new File(root,"metadata/.plugins/org.eclipse.debug.core/.launches/(Before TA fwk 1.7.0) Run selected test(s).launch").getPath());
		fileListWithPlaceHolder.add(new File(root,"metadata/.plugins/org.eclipse.debug.core/.launches/(Before TA fwk 1.7.0) Run test list.launch").getPath());
		
		//jdk
		fileListWithPlaceHolder.add(new File(root,"metadata/.plugins/org.eclipse.core.runtime/.settings/org.eclipse.jdt.core.prefs").getPath());
		fileListWithPlaceHolder.add(new File(root,"metadata/.plugins/org.eclipse.core.runtime/.settings/org.eclipse.jdt.launching.prefs").getPath());
		fileListWithPlaceHolder.add(new File(root,"metadata/.plugins/org.eclipse.jdt.launching/.install.xml").getPath());
		fileListWithPlaceHolder.add(new File(root,"metadata/.plugins/org.eclipse.jdt.launching/libraryInfos.xml").getPath());

		//Placeholders for eclipse configuration
		root = new File(new File(taToolboxPath),eclipseSubPath);
		fileListWithPlaceHolder.add(new File(root,"p2/profiles.info").getPath());
		fileListWithPlaceHolder.add(new File(root,"p2/pools.info").getPath());
		fileListWithPlaceHolder.add(new File(root,"eclipse.ini").getPath());
		fileListWithPlaceHolder.add(new File(root,"configuration/.settings/org.eclipse.ui.ide.prefs").getPath());
		LOGGER.info(" "+ fileListWithPlaceHolder.size() + " files to process.. ");
	}

/**
 * Worspace installer Entry point
 * @param args: paths of software installed
 */
	public static void main(String... args) {

	   LOGGER.info(" \n*****************************************************  ");
		LOGGER.info("    Worskspacer installer starting: 'main' IN");
		try {
			new WorkspaceInstaller(args);
		} catch (IOException e) {
			LOGGER.error("WorkspaceInstaller::main IOException ",e);
		} catch (URISyntaxException e) {
			LOGGER.error("WorkspaceInstaller::main URISyntaxException " ,e);
		}
	}

	/*
	 * parses the command line, looking for options. It will return it's value
	 * if found, null if not found. Wont crash when random string are passed
	 * along the arg line as long as a valid options always are paired with
	 * their value.
	 * 
	 * @param option the option we look for
	 * 
	 * @param args the command line
	 * 
	 * @return the value if found, null if not found
	 * 
	 * @throws IllegalArgumentException if an option was found but its value was
	 * not.
	 */
	private String getOpt(String option, String... args) {
		Iterator<String> iter = Arrays.asList(args).iterator();

		while (iter.hasNext()) {
			String buffer = iter.next();
			if (buffer.equals(option)) {
				if (!iter.hasNext()) {
					String msg = "option " + option + " has incorrect value";
					LOGGER.error("Parse argument line option => " + msg);
					throw new IllegalArgumentException(msg);
				} else {
					String value = iter.next();
					LOGGER.info("option " + option + " has value " + value);
					return value;
				}
			}
		}

		return null;

	}

}

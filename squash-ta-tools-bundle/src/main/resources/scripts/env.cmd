@REM
@REM     This file is part of the Squashtest platform.
@REM     Copyright (C) 2011 - 2019 Henix
@REM
@REM     See the NOTICE file distributed with this work for additional
@REM     information regarding copyright ownership.
@REM
@REM     This is free software: you can redistribute it and/or modify
@REM     it under the terms of the GNU Lesser General Public License as published by
@REM     the Free Software Foundation, either version 3 of the License, or
@REM     (at your option) any later version.
@REM
@REM     this software is distributed in the hope that it will be useful,
@REM     but WITHOUT ANY WARRANTY; without even the implied warranty of
@REM     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
@REM     GNU Lesser General Public License for more details.
@REM
@REM     You should have received a copy of the GNU Lesser General Public License
@REM     along with this software.  If not, see <http://www.gnu.org/licenses />.
@REM

@echo off

cd ..

:: Positionnement des variables d'environnement

set SQUASH_TA_HOME=%cd%
set SQUASH_TA_DRIVE=%SQUASH_TA_HOME:~0,2%

set JAVA_HOME=$jdkPath
set MVN_HOME=%SQUASH_TA_HOME%\${tools.maven.name}
set SAHI_HOME=%SQUASH_TA_HOME%\${tools.sahi.name}\userdata\bin
set ECLIPSE_HOME=%SQUASH_TA_HOME%\${tools.eclipse.name}
set JAILER_HOME=%SQUASH_TA_HOME%\${tools.jailer.name}

:: Ajout des variables d'environnement au path
path=%JAVA_HOME%\bin;%MVN_HOME%\bin;%ECLIPSE_HOME%;%path%

:: Impression
echo [INFO] ------------------------------------------------------------------------
echo [INFO] Mise en place des variables d'environnement
echo [INFO] ------------------------------------------------------------------------
echo [INFO] SQUASH_TA_HOME = %SQUASH_TA_HOME%
echo [INFO] JAVA_HOME = %JAVA_HOME%
echo [INFO] MVN_HOME = %MVN_HOME%
echo [INFO] SAHI_HOME = %SAHI_HOME%
echo [INFO] ECLIPSE_HOME = %ECLIPSE_HOME%
echo [INFO] JAILER_HOME = %JAILER_HOME%
@REM
@REM     This file is part of the Squashtest platform.
@REM     Copyright (C) 2011 - 2019 Henix
@REM
@REM     See the NOTICE file distributed with this work for additional
@REM     information regarding copyright ownership.
@REM
@REM     This is free software: you can redistribute it and/or modify
@REM     it under the terms of the GNU Lesser General Public License as published by
@REM     the Free Software Foundation, either version 3 of the License, or
@REM     (at your option) any later version.
@REM
@REM     this software is distributed in the hope that it will be useful,
@REM     but WITHOUT ANY WARRANTY; without even the implied warranty of
@REM     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
@REM     GNU Lesser General Public License for more details.
@REM
@REM     You should have received a copy of the GNU Lesser General Public License
@REM     along with this software.  If not, see <http://www.gnu.org/licenses />.
@REM



SET JAR=%~1
SET TA_DIRECTORY=%~2
SET WKS=%~3
SET JDK=%~4
SET ECLIPSE=%~5
SET MVN=%~6

echo
echo JAR : %JAR%
echo
echo TA_DIRECTORY : %TA_DIRECTORY%
echo
echo WORKSPACE : %WKS% 
echo
echo JDKPath : %JDK% 
echo
echo ECLIPSE_SUBFOLDER : %ECLIPSE%
echo
echo MAVEN_SUBFOLDER : %MVN%
echo
echo Start installation
echo

java -jar "%JAR%" -ta-directory "%TA_DIRECTORY%" -workspace-directory "%WKS%" -jdkPath "%JDK%" -eclipse-subfolder "%ECLIPSE%" -maven-subfolder "%MVN%"

echo
echo Installation done
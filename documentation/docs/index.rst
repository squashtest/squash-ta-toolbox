..
  |    This file is part of the Squashtest platform.
  |    Copyright (C) 2011 - 2019 Henix
  |
  |    See the NOTICE file distributed with this work for additional
  |    information regarding copyright ownership.
  |
  |    This is free software: you can redistribute it and/or modify
  |    it under the terms of the GNU Lesser General Public License as published by
  |    the Free Software Foundation, either version 3 of the License, or
  |    (at your option) any later version.
  |
  |    this software is distributed in the hope that it will be useful,
  |    but WITHOUT ANY WARRANTY; without even the implied warranty of
  |    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  |    GNU Lesser General Public License for more details.
  |
  |    You should have received a copy of the GNU Lesser General Public License
  |    along with this software.  If not, see <http://www.gnu.org/licenses />.

.. Squash TF Eclipse Toolbox documentation master file, created by
   sphinx-quickstart on Fri Feb  8 16:49:56 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

#########################
Squash TF Eclipse Toolbox
#########################

.. toctree::
   :numbered:
   :hidden:
   :maxdepth: 2

Bla bla

